# Rare events modelling - tree mortality analyses

## Description
This repository contains the code used in the analyses described in:  
Modelling rare events from environmental data: guidelines for prediction and inference using tree mortality as a case study. Submitted to *Methods in Ecology and Evolution*.  

## Data
The data used in this article originates from the Swiss National Forest Inventory. These data are protected by the swiss law and thus cannot be shared publicly. However, they can be provided free of charge within the scope of a contractual agreement (http://www.lfi.ch/dienstleist/daten-en.php).   

In order to run the code, articifial data were created based on these original data. These artificial data are stored in the subdirectory "Data" and are described below.  

### Dataframe "dat_artificial.RDS" 
This data frame contains these artificial data derived from the original dataset. They consist in a subset of the spruce dataset containing the data from 200 randomly selected sample plots (n trees = 2176), in which noise was artificially added to the values of each explanatory variable. Plot IDs were also changed. Variables calculation depends on the combination of three consecutive inventories A, B and C. All explanatory variables are already scaled. For more details on the calculation of the explanatory variables, see directly the article. 
The dataframe contains one row per tree and the following columns:  
- STATUS    : 0 if the tree has survived between inventories B and C, 1 if it has died  
- PLOT_ID   : fake ID of the sample plot in which the tree is located  
- DELTA_T   : (not scaled) time interval between inventories B and C, in number of vegetation years (Oct one year to Sept the next year)  
- REP_FAC   : (not scaled) Representation factor of the tree at inventory B = how many trees are represented in one hectare by the tree into consideration. REP_FAC depends on tree size and sample plot area (which can be reduced by plot boudaries such as forest edge, road, river...)  
- DBH       : (scaled) Diameter at Breast Height in cm at inventory B  
- RELBAI    : (scaled) Relative Basal Area Increment of the tree between inventories A and B  
- RELBAL    : (scaled) Relative Basal Area of trees larger than the tree into consideration in cm  
- BA        : (scaled) Basal Area of the sample plot  
- NPH.log   : (scaled) log of Number of stems per hectare of the plot  
- precip    : (scaled) weighted mean of vegetation-year precipitation sum over the 20 years leading to inventory C  
- temper    : (scaled) weighted mean of vegetation-year mean temperature over the 20 years leading to inventory C  
- SPEI      : (scaled) drought variable: integral below threshold of -0.8 of SPEI index over the 20 years leading to inventory C  
- SLOPE.sqrt: (scaled) square root of the slope of the plot  
- BEERS     : (scaled) Beers aspect corrected for slope of the plot  

### Dataframe "dat_for_inference.RDS" 
This dataframe has a similar structure are the previously described one, and is used in the analyses to make predictions in the context of assessing the inference capacity of the tested approaches. It does not contain the column STATUS as it is the response variable that will be predicted, nor the columns PLOT_ID and REP_FAC that are not needed to make predictions.
It contains the additional column FOCUS that specifies which variables are being varied, e.g. if FOCUS = DBH, all variables are constant at their mean values (on the original dataset) and only DBH varies.
This file exists also in .txt format for the MLP analyses in Python.
Note that contrary to the artificial dataset used to run the analyses, this dataframe is the exact same as the one used in the original analyses.

## How to run the code
The directory "Code" contains the code performing the analyses. 
Before running any code, the Python environment should be set up as it is used to fit the MLP models.

### 1. Setting up Python environment for MLP models

a. Install Miniconda by following instructions from here: https://docs.anaconda.com/miniconda/. (Optional) Other solutions for creating a Python environment can be also used. The instraction bellow are based on a Miniconda setup.  

b. Start Anaconda Prompt and create and activate your working environment: 
>> conda create -n treemort python   
>> conda activate treemort   

c. Install required libraries. To install PyTorch, follow instructions from https://pytorch.org/ and choose installation according to your system. The PyTorch version 2.4.1 or earlier should be used to ensure code reproducibility.  Please note that to use the GPU version of PyTorch, the CUDA library should be preinstalled (for more details, see here: https://docs.nvidia.com/cuda/cuda-installation-guide-microsoft-windows/index.html). To run this code, the CPU version of PyTorch might be enough: 
 >> conda install numpy pandas   
 >> conda install pytorch==2.4.1 torchvision torchaudio cpuonly -c pytorch  
 >> pip install -U scikit-learn  
 
d. Set path to your working directory. Windows example for a directory 'D:/tree-mortality/':  
 >> D: # change to D:/  
 >> cd tree-mortality  
     
e. If you want to run the MLP analyses independently from the others, the Python code can be run as follow: Run Python code  
 >> python Code/mlp/run-MLP.py  

However, once the Python environment has been set up following steps 1 to 4, the analyses can be run directly from the R code, which will call the Python code. 

### 2. Run file "analyses.R"
This R-script performs all the analyses, including the ones related to the MLP. It should be run from beginning to end.

### 3. Folder "helping functions": do not need to be run
The R-scripts located in this folder should not be run. They are called directly from the analyses R-script and contain various functions that are needed throughout the analyses.

## Licence
See Licence file in this repository.

## Authors


## Contact



