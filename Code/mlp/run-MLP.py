#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import shutil
import numpy as np
import pandas as pd
import torch

from utils import create_folds, mlp_train, mlp_predict


# data path 
data_path = 'Data'
data_folds = 'df_folds.txt'
data_path_new = data_path+'/dat_for_inference.txt'

# output path
out_dir = 'Output_MLP'
mod_path = out_dir+'/models/'
preds_path = out_dir+'/predictions_on_folds/'
out_path_new = out_dir+'/predictions_on_newdata/'


# create output directories for modelling results and predictions
for i in [out_dir, mod_path, preds_path, out_path_new]:
    if os.path.exists(i) == False:
        os.mkdir(i)


# SPLIT DATA INTO FOLDS
create_folds(data_dir=data_path, file = data_folds)

# TRAIN MODELS
# set number of model runs. All models will be used to create an final ensemble model
n_runs = 25 
# set device 
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

print('----- MLP: MODEL TRAINING ------')
for i in range(0, n_runs): 
    
    # no weighting
    mlp_train(device = device, run = i, 
              weighted=False, 
              p_weight = None, 
              data_path=data_path, 
              save_path = mod_path)
    
    # with weighting, positive class weight = 10
    mlp_train(device = device, run = i, 
              weighted=True, 
              p_weight = 10, 
              data_path=data_path, 
              save_path = mod_path)
    
    # with weighting, positive class weight = 20
    mlp_train(device = device, run = i, 
              weighted=True, 
              p_weight = 20, 
              data_path=data_path, 
              save_path = mod_path)
    
print('----- MLP: TRAINING COMPLITED ------')


# PREDICT ON FOLDS
print('----- MLP: PREDICTING ON FOLDS ------')
models = os.listdir(out_dir+'/models')
n_folds = len(os.listdir(data_path+'/folds_mlp'))
for m in range(len(models)):
    print(models[m])
    
    # create output directory
    if os.path.exists(preds_path+models[m]) == False:
        os.mkdir(preds_path+models[m])
            
    for f in range(n_folds): 
        csv_path = data_path+'/folds_mlp/'+'fold'+str(f+1)+'.csv'
        out_file = preds_path+models[m]+'/'+'fold'+str(f+1)+'_m6.txt'
        
        for r in range(0, n_runs): # runs
            model_path = mod_path+models[m]+'/run'+str(r)+'/'+'fold'+str(f+1)+'/'
            if r == 0:
                pred_probs = mlp_predict(model_dir=model_path, 
                                         data_path = csv_path, 
                                         prediction_on_folds=True)
            else: 
                pred_probs += mlp_predict(model_dir=model_path, 
                                          data_path = csv_path,
                                          prediction_on_folds=True)
        # average predicted probabilities        
        pred_probs = pred_probs/n_runs
        # save output
        np.savetxt(out_file, pred_probs)
  
        
# PREDICT ON NEW DATA
print('----- MLP: PREDICTING ON NEW DATA ------')
for k in range(0,3):
    model_name = models[k]
    out_file = models[k]+'.txt'
    
    d_probs = []
    folds = []
    for i in range(n_folds): 
        
        folds.append('fold'+str(i+1))
        for r in range(0, n_runs): # runs
            
            # set model path    
            model_path = mod_path+models[k]+'/run'+str(r)+'/fold'+str(i+1)+'/'
            print(model_path)
            if r == 0:
                pred_probs_new = mlp_predict(model_dir=model_path, 
                                             data_path = data_path_new)
            else:
                pred_probs_new += mlp_predict(model_dir=model_path, 
                                              data_path = data_path_new)
        
        # average probabilites
        pred_probs_new = pred_probs_new/n_runs
        d_probs.append((pred_probs_new))
    
    # add results into output table    
    d_probs = np.hstack(d_probs)
    d_probs= pd.DataFrame(d_probs, columns=folds)
    # save output table
    st_name = out_path_new+out_file
    d_probs.to_csv(st_name, sep=' ', mode='w') 
    
    
# remove temporary files
shutil.rmtree(data_path+'/folds_mlp')
    
# end ---



