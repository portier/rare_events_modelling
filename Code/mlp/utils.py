#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import glob
from random import sample

import numpy as np
from numpy import vstack
import pandas as pd

from sklearn.metrics import accuracy_score, roc_auc_score, f1_score
from sklearn.metrics import confusion_matrix

import torch
from torch import Tensor
from torch.nn import Linear, ReLU, BCEWithLogitsLoss, BatchNorm1d, Module
from torch.utils.data import Dataset, DataLoader
from torch.optim import SGD, lr_scheduler
from torch.nn.init import kaiming_uniform_
from torch.nn.init import xavier_uniform_

    
# split data into folds
def create_folds(data_dir='Data', file = 'df_folds.txt'):
    
    # list of predictors
    var_list = ['STATUS', 'DBH', 'RELBAI', 'RELBAL', 'BA',
                'NPH.log', 'precip', 'temper', 'SLOPE.sqrt', 
                'BEERS', 'DELTA_T', 'SPEI']
    
    # create temporary directory for data
    fold_dir = data_dir+'/folds_mlp'
    if os.path.exists(fold_dir) != True:
        os.mkdir(fold_dir)
    # read data
    df = pd.read_csv(data_dir+'/'+file, sep=" ")
    
    # split data into folds
    for i in range(20):
        d = df.copy()
        d = d[d['fold'] == (i+1)]
        d = d[var_list]
        if len(d)>0:
            d.to_csv(fold_dir+'/fold'+str(i+1)+'.csv', index=False)
            

# Create a custom CSVDataset loader
class CSVDataset(Dataset):
    # Constructor for initially loading
    def __init__(self, files, 
                 normalize=True, 
                 min_values=None, 
                 max_values=None):
        
        df = pd.concat(map(pd.read_csv, files), ignore_index=True)
       
        # Store the inputs and outputs
        self.X = df.values[:, 1:len(df.columns)]  # exclude outcome variable (here, STATUS) 
        self.y = df.values[:, 0] # assuming your outcome variable is in the first column
        self.X = self.X.astype('float32')
        self.y = self.y.astype('float32')
        self.y = self.y.reshape((len(self.y), 1))
            
        if normalize == True:
            self.X = np.apply_along_axis(NormalizeData, 1, self.X, 
                                         min_values, max_values)
                
    # Get the number of rows in the dataset
    def __len__(self):
        return len(self.X)
    # Get a row at an index
    def __getitem__(self,idx):
        return [self.X[idx], self.y[idx]]
    
# normalize data
def NormalizeData(data, min_values, max_values):
    return (data - min_values) / (max_values - min_values)


# calculate min and max values for each column in a table
def get_min_max_values(fls, save_path):
    td = pd.concat(map(pd.read_csv, fls), ignore_index=True)
    min_values = td.apply(np.min, axis=0).astype('float32')
    max_values = td.apply(np.max, axis=0).astype('float32')
    
    np.savetxt(save_path+'/min_values.txt', min_values)
    np.savetxt(save_path+'/max_values.txt', max_values)
        
    return min_values, max_values


# split data into train/val/test sets
def split_data(fls, test_id = 1, nfiles_val = 2):
    fls_train = fls.copy()
    fls_test = fls_train[test_id]
    fls_train.remove(fls_test)

    # validation files
    fls_val = sample(fls_train, nfiles_val)
    
    # train files
    for i in range(nfiles_val):
        # exclufing files used for validation
        fls_train.remove(fls_val[i]) 
        
    fls_test = [fls_test] 
    if nfiles_val == 1:
        fls_val = [fls_val]
    
    return fls_train, fls_val, fls_test


# Design neural network
class MLP(Module):
    def __init__(self, n_inputs):
        super(MLP, self).__init__()
        # First hidden layer
        self.hidden1 = Linear(n_inputs, 20)
        kaiming_uniform_(self.hidden1.weight, nonlinearity='relu')
        self.act1 = ReLU()
        # Second hidden layer
        self.hidden2 = Linear(20, 10)
        kaiming_uniform_(self.hidden2.weight, nonlinearity='relu')
        self.act2 = ReLU()
        # Third hidden layer
        self.hidden3 = Linear(10,1)
        xavier_uniform_(self.hidden3.weight)
        
        self.batchnorm1 = BatchNorm1d(20)
        self.batchnorm2 = BatchNorm1d(10)
    
    def forward(self, X):
        #Input to the first hidden layer
        X = self.hidden1(X)
        X = self.act1(X)
        X = self.batchnorm1(X)
        # Second hidden layer
        X = self.hidden2(X)
        X = self.act2(X)
        X = self.batchnorm2(X)
        # Third hidden layer
        X = self.hidden3(X)
        return X
    

# Create training loop based off our custom class
def train_model(train_data, val_data, model, device, 
                epochs=100, 
                pos_weight = None, 
                save_path = None):
    
    # Define your optimisation function for reducing loss when weights are calculated 
    # and propogated through the network
    # specify loss function
    criterion = BCEWithLogitsLoss(pos_weight=pos_weight)
    # specify optimiser
    optimizer = SGD(model.parameters(), 
                    lr=0.01, 
                    momentum=0.9, 
                    weight_decay=5e-4, 
                    nesterov=True)
    
    scheduler = lr_scheduler.StepLR(optimizer, step_size=25, gamma=0.1)
    model = model.to(device)

    train_loss = []
    val_loss = []
    temp_val_loss = 9999
    
    for epoch in range(epochs):
        print('Epoch {}/{}'.format(epoch+1, epochs))
        print('-' * 10)
        
        epoch_loss_train = 0.0
        model.train()
        # Iterate through training data loader
        for i, (inputs, targets) in enumerate(train_data):

            inputs, targets = inputs.to(device), targets.to(device)
            
            optimizer.zero_grad()
            outputs = model(inputs)
            loss = criterion(outputs, targets)
                
            loss.backward()
            optimizer.step()
            
            # Print statistics
            epoch_loss_train += loss.item()
            if i == len(train_data)-1:
                print('Training loss after mini-batch %5d: %.3f' %
                      (i + 1, epoch_loss_train / len(train_data)))
                train_loss.append(epoch_loss_train / len(train_data))
            
        model.eval()
        epoch_loss_val = 0.0
        for i, (inputs, targets) in enumerate(val_data):
            inputs, targets = inputs.to(device), targets.to(device)
            outputs = model(inputs)
            # loss function
            loss = criterion(outputs, targets)
            
            # Print statistics
            epoch_loss_val += loss.item()
            if i == len(val_data)-1:
                print('Validation loss after mini-batch %5d: %.3f' %
                      (i + 1, epoch_loss_val / len(val_data)))
                val_loss.append(epoch_loss_val / len(val_data))
        
        if epoch_loss_val / len(val_data) < temp_val_loss: #and epoch > 20
            temp_val_loss = epoch_loss_val / len(val_data)
            torch.save(model.state_dict(), save_path+'best_model.pth')
            
        scheduler.step()
        print('Epoch-{0} lr: {1}'.format(epoch, optimizer.param_groups[0]['lr']))
    
    # save training and validation losses
    np.savetxt(save_path+'Validation_Loss.txt', val_loss)
    np.savetxt(save_path+'Training_Loss.txt', train_loss)
    return model

# calculate evaluation metrics  
def get_statistics(actuals, preds):
    
    # Calculate metrics
    cm = confusion_matrix(actuals, preds)
    # Get descriptions of tp, tn, fp, fn
    tn, fp, fn, tp = cm.ravel()
    
    metrics = [['accuracy', accuracy_score(actuals, preds)],
               ['AU_ROC', roc_auc_score(actuals, preds)],
               ['f1_score', f1_score(actuals, preds)],
               ['sensitivity', tp / (tp + fn)],
               ['specificity', tn / (tn + fp)],
               ['TP', tp],
               ['TN', tn],
               ['FP', fp], 
               ['FN', fn]]
    
    df = pd.DataFrame(metrics, columns=['Metric', 'Value'])
    return df


# evaluating model perfomance
def evaluate_model(test_dl, model, save_path_stats, 
                   save_path_preds = 'predictions'):
    preds_probs = []
    preds = []
    actuals = []
    for (i, (inputs, targets)) in enumerate(test_dl):
        #Evaluate the model on the test set
        yhat = model(inputs)
        yhat = torch.sigmoid(yhat)
        #Retrieve a numpy weights array
        yhat_probs = yhat.detach().numpy()
        # Extract the weights using detach to get the numerical values in an ndarray, instead of tensor
        actual = targets.numpy()
        actual = actual.reshape((len(actual), 1))
        # Round to get the class value i.e. sick vs not sick
        yhat = yhat_probs.round()
        # Store the predictions in the empty lists initialised at the start of the class
        preds_probs.append(yhat_probs)
        preds.append(yhat)
        actuals.append(actual)
    
    # Stack the predictions and actual arrays vertically
    preds_probs, preds, actuals = vstack(preds_probs), vstack(preds), vstack(actuals)
    
    # Calculate and save evaluation metrics
    metrics = get_statistics(actuals, preds)
    metrics.to_csv(save_path_stats+'TEST_Statistics.txt', 
                   header=None, index=None, sep=' ', mode='w')
    return metrics


# calculate positive class weight
def calculate_pos_weight(fls, Y_column = 'STATUS'):
    td = pd.concat(map(pd.read_csv, fls), ignore_index=True)
    pos_class_weight = len(td[td[Y_column]==0]) / len(td[td[Y_column]==1])
    pos_class_weight = round(pos_class_weight)
    return [pos_class_weight]


# train model
def mlp_train(device, run = 0,
                   weighted = True, 
                   p_weight = None,
                   data_path = None, 
                   save_path = None):
    
    # create output directory
    if weighted==True:
        model_type = 'weighted-' + str(p_weight)
    else:
        model_type = 'no_weight'

    # check if the output directory exists
    if os.path.exists(save_path+model_type+'/') == False:
        os.mkdir(save_path+model_type+'/')
        
    # check if the output directory exists
    if os.path.exists(save_path+model_type+'/'+'run'+str(run)) == False:
        os.mkdir(save_path+model_type+'/'+'run'+str(run))
    
    # data
    fls = glob.glob(data_path + '/folds_mlp/*.csv')
    print(fls)

    for i in range(len(fls)): 
        # get name of test file
        test_ID = fls[i]
        test_ID = os.path.basename(test_ID)
        test_ID = os.path.splitext(test_ID)[0]
        print(test_ID)
        
        # create out directory for models
        out_path_ = save_path+model_type+'/'+'run'+str(run)+'/'+test_ID+'/'
        if os.path.exists(out_path_) == False:
                os.mkdir(out_path_)
        
        # check if the model exists and if it was trained completelly
        if len(glob.glob(out_path_+'*')) == 6: 
            print('Model exists')
        else:    
            # split files for training, validation and testing
            train_fls, val_fls, test_fls = split_data(fls, test_id = i)
            
            # get min max values for data normalisation
            min_values, max_values = get_min_max_values(train_fls, 
                                                        save_path = out_path_)
            # take into account class imblance by introducing class weights  
            if weighted: 
                if p_weight == None: 
                    # get positive class weight from data 
                    pos_class_weight = Tensor(calculate_pos_weight(train_fls, Y_column = 'STATUS')) 
                else:
                    pos_class_weight = Tensor([p_weight]) 
                pos_class_weight = pos_class_weight.to(device)
            else: 
                pos_class_weight = None
            
            # prepare training data
            train_dataset = CSVDataset(train_fls, 
                                       normalize=True, 
                                       min_values = min_values[1:len(min_values)], 
                                       max_values = max_values[1:len(min_values)])
            train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True, drop_last=True)
            
            # prepare validation data
            val_dataset = CSVDataset(val_fls, 
                                     normalize=True, 
                                     min_values = min_values[1:len(min_values)], 
                                     max_values = max_values[1:len(min_values)])
            val_loader = DataLoader(val_dataset, batch_size=32, shuffle=True)
        
            # prepare test data
            test_dataset = CSVDataset(test_fls, 
                                      normalize=True, 
                                      min_values = min_values[1:len(min_values)], 
                                      max_values = max_values[1:len(min_values)])
            test_loader = DataLoader(test_dataset, batch_size=1000, shuffle=True)

            # load and callibrate model
            model = MLP(11)
            model_eval = MLP(11)
            
            # train model
            train_model(train_loader, val_loader, model, 
                        pos_weight = pos_class_weight, 
                        device = device,
                        save_path = out_path_)

            # evaluate best model
            if torch.cuda.is_available():
                model_eval.load_state_dict(torch.load(out_path_+'best_model.pth', 
                                                      #weights_only=True, 
                                                      map_location=torch.device('cuda:0')))
            else: 
                model_eval.load_state_dict(torch.load(out_path_+'best_model.pth', 
                                                      #weights_only=True, 
                                                      map_location=torch.device('cpu')))
            model_eval.eval()
            evaluate_model(test_loader, 
                           model_eval, 
                           save_path_stats=out_path_)


# -- INFERENCE ---
# predict model
def mlp_predict(model_dir, 
                data_path = False, 
                prediction_on_folds=False):
    
    # load min and max values
    min_values = np.loadtxt(model_dir+'min_values.txt')
    max_values = np.loadtxt(model_dir+'max_values.txt')
    
    # list of predictors
    var_list = ['STATUS', 'DBH', 'RELBAI', 'RELBAL', 'BA',
                'NPH.log', 'precip', 'temper', 
                'SLOPE.sqrt', 'BEERS', 'DELTA_T', 'SPEI']
    
    # import data
    if data_path != False:
        _, file_extension = os.path.splitext(data_path)
        
        if file_extension == '.csv':
            data = pd.read_csv(data_path)
        if file_extension == '.txt':
            data = pd.read_csv(data_path, sep=' ')

    # get set of predictors
    if prediction_on_folds:    
        data = data[var_list]
        X = data.values[:, 1:len(data.columns)]
    else: 
        data = data[var_list[1:]]
        X = data.values[:, 0:len(data.columns)]

    # convert data to float32
    X = X.astype('float32')
    # normalize data
    X = np.apply_along_axis(NormalizeData, 1, X, 
                            min_values[1:len(min_values)], 
                            max_values[1:len(max_values)])
    # convert to a tensor
    X = torch.Tensor(X)
    
    # import model
    model_path = model_dir+'best_model.pth'
    model = MLP(11)
    if torch.cuda.is_available():    
        model.load_state_dict(torch.load(model_path, 
                                         #weights_only=True, 
                                         map_location=torch.device('cuda:0')))
    else:
        model.load_state_dict(torch.load(model_path, 
                                         #weights_only=True, 
                                         map_location=torch.device('cpu')))
    
    # predict
    preds = model(X)
    preds = torch.sigmoid(preds).detach().numpy()
    
    return preds

# end -- 
